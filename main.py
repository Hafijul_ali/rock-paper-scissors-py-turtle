from turtle import Screen,Turtle 
from random import choice,randint
from time import sleep

   
screen=Screen()
screen.setup(1.0,1.0)
SCR_HEIGHT,SCR_WIDTH=screen.screensize() 
screen.setworldcoordinates(0,0,SCR_HEIGHT,SCR_WIDTH)
screen.delay(0)
screen.colormode(255)
screen.bgcolor(150,0,120)
screen.title("Rock Paper Scissors")

SCR_BG_COLOR=screen.bgcolor()
TEXT_COLOR=tuple([int(255-x) for x in SCR_BG_COLOR])
OFF_SET=50
BUTTON_WIDTH=4.0
BUTTON_LENGTH=9.0 
FONT_SIZE=15
FONT="AgencyFB"
SHAPE="square"

INTRO_MSG="""
This is a simple Rock,Paper,Scissors game.
Click on the buttons to enter your move, you play against the computer.
Thank you."""

score=0 

class Button(Turtle):
    def __init__(self,name,bg_color,text_color,font_face,size,shape,btn_width,btn_len,x,y):
    	super().__init__() 
    	self.name=name
    	self.pen(fillcolor=bg_color,pencolor=text_color,shown=True,pendown=False)
    	self.shape(shape)
    	self.shapesize(btn_width,btn_len)
    	self.setposition(x,y)
    	self.write(self.name,font=(font_face,size),align="center")

    def glow(self):
        self.fillcolor(randint(0,255),randint(0,255),randint(0,255))
        sleep(0.5)
    
    def unglow(self):
        self.fillcolor("")
        
    def click_effect(self):
    	self.glow()
    	sleep(0.5) 
    	self.unglow()
    	
    def display_move(self,x,y):
        global SCR_WIDTH,SCR_HEIGHT,FONT, FONT_SIZE,user_moved,user_move
        Writer.clear()
        self.click_effect()
        Writer.write("Your move : {0}".format(self.name),font=(FONT,FONT_SIZE),align="center")
        sleep(0.5)
        user_move=self.name
        user_moved=True
       
          
    def exit(self,x,y):
        global score,FONT,FONT_SIZE
        self.click_effect()
        Writer.clear()
        Writer.down()
        Writer.write("Your final score was, : {0}".format(score),font=(FONT,FONT_SIZE),align="center")
        sleep(2)
        Writer.clear()
        Writer.write("Thank you for playing",font=(FONT,FONT_SIZE),align="center")
        sleep(2)
        Writer.clear()
        Writer.write("Closing application",font=(FONT,FONT_SIZE),align="center")
        for element in screen.turtles():
            element.clear()
            element.ht()
            sleep(0.5)
        sleep(1)    
        screen.bye()
    
    def info(self,x,y):
        global FONT,FONT_SIZE
        Writer.clear()
        self.click_effect()
        Writer.write(INTRO_MSG,font=(FONT,FONT_SIZE-2),align="center")
        sleep(10)
       


Rock=Button("Rock","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,OFF_SET,OFF_SET)
Paper=Button("Paper","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,SCR_WIDTH//2+OFF_SET,OFF_SET)
Scissors=Button("Scissors","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,SCR_WIDTH+OFF_SET,OFF_SET)#
Help=Button("Help","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,SCR_HEIGHT-OFF_SET,SCR_WIDTH-OFF_SET)
Settings=Button("Settings","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,SCR_HEIGHT//2,SCR_WIDTH-OFF_SET)
Close=Button("Close","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,OFF_SET,SCR_WIDTH-OFF_SET)
Writer=Button("","",TEXT_COLOR,FONT,FONT_SIZE,SHAPE,BUTTON_WIDTH,BUTTON_LENGTH,SCR_HEIGHT//2,SCR_WIDTH//2)
Writer.hideturtle()

def comp_win(score):
    global FONT,FONT_SIZE
    score-=10
    Writer.clear()
    Writer.setposition(SCR_HEIGHT//2,SCR_WIDTH//2)
    Writer.write("Computer wins, score is : {0}".format(score),font=(FONT,FONT_SIZE),align="center")
    return score

def user_win(score):
    score+=10
    Writer.clear()
    Writer.write("User wins, score is : {0}".format(score),font=(FONT,FONT_SIZE),align="center")
    return score
    
user_moved=False

while True:
    user_move=""
    computer_move=" "
    Rock.onclick(Rock.display_move)
    Paper.onclick(Paper.display_move)
    Scissors.onclick(Scissors.display_move)
    Help.onclick(Help.info)
    Close.onclick(Close.exit)
    
    if user_moved:
        computer_move=choice(["Rock","Paper","Scissors"])
        Writer.clear()
        Writer.write("Computer's move : {0}".format(computer_move),font=(FONT,FONT_SIZE),align="center")
        sleep(1)
        user_moved=False
    
    if user_move == computer_move:
        Writer.clear()
        Writer.write("Its a tie, your score is : {0}".format(score),font=(FONT,FONT_SIZE),align="center")
    elif user_move=="Paper" and computer_move=="Scissors":
        score=comp_win(score)
    elif user_move=="Rock" and computer_move=="Paper":
        score=comp_win(score)
    elif user_move=="Scissors" and computer_move=="Rock":
        score=comp_win(score)
    elif user_move=="Paper" and computer_move=="Rock":
        score=user_win(score)
    elif user_move=="Scissors" and computer_move=="Paper":
        score=user_win(score)
    elif user_move=="Rock" and computer_move=="Scissors":
        score=user_win(score)
        
        
        
       
